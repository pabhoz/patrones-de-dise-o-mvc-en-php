<div id="userMenu" class="floatingWindow">

	<ul>
		<li>Mi Perfil</li>
		<li>Mi Carrito</li>
		<li data-option="logout">Cerrar Sesión</li>
	</ul>

</div>

<script>

	$("#userMenu li").click(function(){
		var opt = $(this).data("option");

		switch(opt){
			case "logout":
				$.ajax({
					url: "<?php print(URL); ?>Users/salir/",
					method: "GET"
				}).done(function(){
					location.reload();
				});
			break;
		}
	});

</script>