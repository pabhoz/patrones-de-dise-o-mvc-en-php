<div class="floatingWindow">

	<div class="tab" data-tab="login">
		 <form name="loginForm">
		 	<input type="text" name="username" required placeholder="Username">
		 	<input type="password" name="password" required placeholder="Password">
		 	<input type="submit" value="Entrar">
		 	<div class="lilText">
		 		<span data-tab="register">¿No eres usuario aún?</span>
		 		<span data-tab="fPassword">¿Olvidaste tu password?</span>
		 	</div>
		 </form>
	 </div>

	 <div class="tab" data-tab="register" style="display:none;">
		 <form name="registerForm" autocomplete="off">
		 	<input type="text" name="username" required placeholder="Username">
		 	<input type="email" name="email" required placeholder="email">
		 	<input type="password" name="password" required placeholder="Password">
		 	<input type="password" name="passwordR" required placeholder="Repeat Password">
		 	<input type="submit" value="Registrar">
		 	<div class="lilText">
		 		<span data-tab="login">Cancelar</span>
		 	</div>
		 </form>
	 </div>

	 <div class="tab" data-tab="fPassword" style="display:none;">
		 <form name="rPasswordForm">
		 	<div class="msg">Enviaremos un correo electrónico con las instrucciones para definir una nueva contraseña.</div>
		 	<input type="text" name="email" required placeholder="email">
		 	<input type="submit" value="Enviar">
		 	<div class="lilText">
		 		<span data-tab="login">Cancelar</span>
		 	</div>
		 </form>
	 </div>
</div>

<script>
	var readyToSend = false;
	var errorMsg = "";

	$(".floatingWindow form .lilText span").click(function(e){

		var trigger = $(this);
		var tab = trigger.data("tab");
		var curTab = trigger.parent().parent().parent().data("tab");
		
		$(".floatingWindow .tab[data-tab='"+curTab+"']").css("display","none");
		$(".floatingWindow .tab[data-tab='"+tab+"']").css("display","block");

	});

	$(".floatingWindow form input").on("blur keyup keydown",function(e){
		//console.log("excecuting");
		var form = $(this).parent()[0];

		switch(form.name){
			case "loginForm":
				//console.log("lógica del login");
			break;
			case "registerForm":
				
				/*** email & username logic***/
				if(e.type == "blur"){
					//console.log("validando usuario y correo");
					var username = form.querySelector("[name=username]");
					validateUnique(username,"Users","verificarUsername","username");

					var email = form.querySelector("[name=email]");
					validateUnique(email,"Users","verificarCorreo","email");
				}
				/*** Passwords logic ****/
				var pass = form.querySelector("[name=password]");
				var rPass = form.querySelector("[name=passwordR]");

				if( pass.value != '' && rPass.value != ''){
					if(pass.value == rPass.value){
						$(pass).removeClass("incorrecto").addClass("correcto");
						$(rPass).removeClass("incorrecto").addClass("correcto");

						readyToSend = true;

					}else{
						$(pass).removeClass("correcto").addClass("incorrecto");
						$(rPass).removeClass("correcto").addClass("incorrecto");

						readyToSend = false;
						errorMsg = "Los passwords no coinciden";
					}
				}
			break;
			case "rPasswordForm":
				console.log("lógica del rPassword");
			break;
		}
	})

	$(".floatingWindow form").submit(function(e){
		e.preventDefault();

		var form = $(this)[0];
		var data = {}

		//Trigger ejecutando un blur en cada input
		var inputs = form.querySelectorAll("input");
		$(inputs).each(function(){
			if($(this).hasClass("incorrecto")){
				readyToSend = false;
			}
		});

		if(form.name == "loginForm"){
			readyToSend = true;
		}

		if(readyToSend){
			data = processForm(form);
			console.log(data);

			switch(form.name)
			{
				case "loginForm":
					//Envío asincrono
					$.ajax({
						url:"<?php print(URL);?>Users/entrar/",
						method:"POST",
						data: data
					}).done(function(r){
						if(JSON.parse(r)){

							var r = JSON.parse(r);
							console.log(r);
							if(r.error == 0){
								location.reload();
							}else{
								alert("Username or Password incorrect");
							}
						}
						
					});
				break;
				case "registerForm":
					//Envío asincrono
					$.ajax({
						url:"<?php print(URL);?>Users/registrar/",
						method:"POST",
						data: data
					}).done(function(r){
						if(JSON.parse(r)){
							var r = JSON.parse(r);
							if(r.error == 0){
								alert(r.msg);
								location.reload();
							}else{
								alert(r.msg);
							}
						}
						
					});
				break;
			}

		}else{
			alert(errorMsg);
		}
		
		return false;
	});

	function validateUnique(who,controller,method,name){

		if(who.value != ''){
			$.ajax({
				url:"<?php print(URL); ?>"+controller+"/"+method+"/"+who.value,
				method:"GET"
			}).done(function(r){
				if(r != 0){
					$(who).removeClass("correcto").addClass("incorrecto");
					readyToSend = false;
					errorMsg = name+" existente en la base de datos";
				}else{
					$(who).removeClass("incorrecto").addClass("correcto");
					readyToSend = true;
				}
			});
		}
				
	}

	function processForm(form){

		var inputs = form.querySelectorAll("input");
		var data = {};
		$(inputs).each(function(){
			var input = $(this);
			if(input.context.type != "submit"){
				var attr = input.context.name;
				data[attr] = input.context.value; 
			}
		});

		return data;
	}

</script>